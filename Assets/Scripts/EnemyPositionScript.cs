﻿using UnityEngine;
using System.Collections;

public class EnemyPositionScript : MonoBehaviour
{

	public float WireSphereRadius = 1f;
	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void Update ()
	{
	}

	void OnDrawGizmos ()
	{
		Gizmos.DrawWireSphere (transform.position, WireSphereRadius);
	}
}