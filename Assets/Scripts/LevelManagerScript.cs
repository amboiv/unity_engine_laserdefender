﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManagerScript : MonoBehaviour
{
	public void LoadLevel (string name)
	{
		SceneManager.LoadScene (name);
	}

	public void LoadNextLevel ()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	public void QuitRequest ()
	{
		Application.Quit ();
	}

	/*TODO: Change method 
	public void BrickDestroyed ()
	{
		if (BrickScript.BreakableCount <= 0)
		{	
			LoadNextLevel ();
		}
	}*/
}
