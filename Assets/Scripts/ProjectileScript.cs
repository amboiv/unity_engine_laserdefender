﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour
{
	public int Damage = 100;

	// Use this for initialization
	void Start ()
	{
	}

	// Update is called once per frame
	void Update ()
	{
	}

	public int GetDamage ()
	{
		return Damage;
	}

	public void Hit ()
	{
		Destroy (gameObject);
	}
}
