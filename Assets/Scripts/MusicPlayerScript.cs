﻿using UnityEngine;
using System.Collections;

public class MusicPlayerScript : MonoBehaviour
{
	private static MusicPlayerScript _instance = null;

	public AudioClip[] musicClips;
	private AudioSource bgMusic;

	// Use this for initialization
	void Awake ()
	{
		if (_instance != null)
		{
			Destroy (gameObject);
		}
		else
		{
			_instance = this;
			DontDestroyOnLoad (gameObject);	
		}
	}

	void Start ()
	{
		bgMusic = GetComponent<AudioSource> ();
		bgMusic.clip = musicClips[0];
		bgMusic.loop = true;
		bgMusic.Play();

	}

	void OnLevelWasLoaded (int level)
	{
		Debug.Log ("MusicPlayer: loaded level " + level);

		if (level == 0)
		{
			bgMusic = GetComponent<AudioSource> ();
		}
		bgMusic.Stop ();
		bgMusic.clip = musicClips[level];
		bgMusic.loop = true;
		bgMusic.enabled = true;
		bgMusic.Play();
	}

	// Update is called once per frame
	void Update ()
	{
	}
}
