﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreDisplayScript : MonoBehaviour
{


	// Use this for initialization
	void Start ()
	{
		Text myText = GetComponent <Text> ();
		myText.text = ScoreKeeperScript.NumberOfPoints.ToString ();
		ScoreKeeperScript.ResetScore ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
