﻿using System;
using UnityEngine;
using System.Collections;

public class EnemySpawnerScript : MonoBehaviour
{
	public GameObject enemyPrefab;
	public float width = 10f;
	public float height = 5f;
	public float formationSpeed = 5f;
	public float SpawnDelay = 0.5f;

	private int leftOrRight = 1;
	private float xMax;
	private float xMin;


	// Use this for initialization
	void Start ()
	{
		float distanceToCamera = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftBoundary = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, distanceToCamera));
		Vector3 rightBoundary = Camera.main.ViewportToWorldPoint (new Vector3 (1f, 0, distanceToCamera));

		xMax = rightBoundary.x;
		xMin = leftBoundary.x;


		SpawnEnemiesUntilFull ();
	}

	public void OnDrawGizmos ()
	{
		Gizmos.DrawWireCube (transform.position, new Vector3 (width, height, 0f));
	}

	// Update is called once per frame
	void Update ()
	{
		transform.position += new Vector3 (leftOrRight * formationSpeed * Time.deltaTime, 0f, 0f);

		float rightEdgeOfFormation = transform.position.x + (0.5f * width);
		float leftEdgeOfFormation = transform.position.x - (0.5f * width);

		if (leftEdgeOfFormation < xMin)
		{
			leftOrRight = 1;
		}
		else if (rightEdgeOfFormation > xMax)
		{
			leftOrRight = -1;
		}

		if (isAllMembersDead ())
		{
			Debug.Log ("Empty Formation");
			SpawnEnemiesUntilFull ();
		}
	}

	bool isAllMembersDead ()
	{
		foreach (Transform childPositionGameObject in transform)
		{
			if (childPositionGameObject.childCount > 0)
			{
				return false;
			}
		}
		return true;
	}

	Transform NextFreePosition ()
	{
		foreach (Transform childPositionGameObject in transform)
		{
			if (childPositionGameObject.childCount == 0)
			{
				return childPositionGameObject;
			}
		}
		return null;
	}

	void SpawnEnemies ()
	{
		foreach (Transform child in this.transform)
		{
			GameObject enemy = (GameObject) Instantiate (enemyPrefab, child.position, Quaternion.identity);
			enemy.transform.parent = child;
		}
	}

	void SpawnEnemiesUntilFull ()
	{
		Transform freePosition = NextFreePosition ();

		if (freePosition)
		{
			GameObject enemy = (GameObject) Instantiate (enemyPrefab, freePosition.position, Quaternion.identity);
			enemy.transform.parent = freePosition;
		}
		if (NextFreePosition ())
		{
			Invoke ("SpawnEnemiesUntilFull", SpawnDelay);
		}
	}
}
