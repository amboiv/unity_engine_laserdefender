﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreKeeperScript : MonoBehaviour
{
	public static int NumberOfPoints { get; set; }
	public Text ScoreText;

	// Use this for initialization
	void Start ()
	{
		ScoreText = GetComponent <Text> ();
		ResetScore ();
		ScoreText.text = NumberOfPoints.ToString ();
	}

	// Update is called once per frame
	void Update ()
	{
	}

	public void Score (int points)
	{
		NumberOfPoints += points;
		ScoreText.text = NumberOfPoints.ToString();
	}

	public static void ResetScore ()
	{
		NumberOfPoints = 0;
	}
}