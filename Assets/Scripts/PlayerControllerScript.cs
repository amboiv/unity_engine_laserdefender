﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class PlayerControllerScript : MonoBehaviour
{
	public float Speed = 5.0f;
	public GameObject PlayerLaserShot;
	public AudioClip[] LaserSounds;
	public float PlayerLaserSpeed = 5f;
	public float FireingRate = .5f;
	public float Health = 250f;

	private float xMin;
	private float xMax;
	private float padding = 1f;

	// Use this for initialization
	void Start ()
	{
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftMost = Camera.main.ViewportToWorldPoint (new Vector3 (0f, 0f, distance));
		Vector3 rightMost = Camera.main.ViewportToWorldPoint (new Vector3 (1f, 0f, distance));
		xMin = leftMost.x + padding;
		xMax = rightMost.x - padding;
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetAxis ("Horizontal") != 0)
		{
			MoveShip ();
		}

		if (Input.GetButtonDown ("Jump"))
		{
			InvokeRepeating("FireStandardLaser", 0.0000001f, 0.5f);
		}
		if (Input.GetButtonUp ("Jump"))
		{
			CancelInvoke("FireStandardLaser");
		}
	}

	void MoveShip ()
	{
		transform.position += new Vector3 (Input.GetAxisRaw ("Horizontal") * Speed * Time.deltaTime, 0f, 0f);


		//Restricting ship to the play space with Mathf.Clamp
		float newX = Mathf.Clamp (transform.position.x, xMin, xMax);
		transform.position = new Vector3 (newX, transform.position.y, transform.position.z);
	}

	void FireStandardLaser ()
	{
		AudioSource.PlayClipAtPoint (LaserSounds[Random.Range(0, LaserSounds.Length)], transform.position);
		GameObject laserShotInstance = (GameObject) Instantiate (PlayerLaserShot, new Vector2(transform.position.x, transform.position.y + .8f), Quaternion.identity);

		/*Hvis denne er med, følger laseren romskipets x-posisjon, selv etter avfyring.
		laserShotInstance.transform.parent = transform;*/
		laserShotInstance.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, PlayerLaserSpeed);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		
		var projectile = other.gameObject.GetComponent<ProjectileScript> ();
		if (projectile && projectile.tag != "PlayerProjectile")
		{
			Debug.Log ("Player was hit");
			Health -= projectile.GetDamage ();
			projectile.Hit ();
			if (Health <= 0)
			{
				Die ();
			}

		}

	}

	void Die ()
	{
		Destroy (gameObject);
		LevelManagerScript levelManager = GameObject.Find ("LevelManager").GetComponent <LevelManagerScript> ();
		levelManager.LoadLevel ("Win Screen");
	}
}
