﻿using UnityEngine;
using System.Collections;

public class EnemyWeakScript : MonoBehaviour
{

	public int Health = 200;
	public int ScoreValue = 150;
	public float ProjectileSpeed;
	public float RandomRateOfFire = 0.5f;
	public GameObject ProjectilePrefab;
	public AudioClip FireSound;
	public AudioClip deathSound;

	private ScoreKeeperScript scoreKeeper;
	 
	// Use this for initialization
	void Start ()
	{
		scoreKeeper = GameObject.Find ("ScoreText").GetComponent <ScoreKeeperScript> ();
		//InvokeRepeating ("Fire", Random.Range (3f, RandomRateOfFire), Random.Range (2f, RandomRateOfFire));
	}

	// Update is called once per frame
	void Update ()
	{
		float probabilityOfFiring = Time.deltaTime * RandomRateOfFire;
		if (Random.value < probabilityOfFiring)
		{
			Fire ();
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		var projectile = other.gameObject.GetComponent <ProjectileScript> ();
		if (projectile && projectile.tag == "PlayerProjectile")
		{
			Health -= projectile.GetDamage();
			projectile.Hit ();
			if (Health <= 0)
			{
				Die ();
			}
		}	
	}

	void Fire ()
	{
		AudioSource.PlayClipAtPoint (FireSound, transform.position);
		GameObject projectileInstance = (GameObject) Instantiate (ProjectilePrefab, new Vector2(transform.position.x, transform.position.y - .8f), Quaternion.identity);
		projectileInstance.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -ProjectileSpeed);
	}

	void Die ()
	{
		scoreKeeper.Score (ScoreValue);
		AudioSource.PlayClipAtPoint (deathSound, transform.position);
		Destroy (gameObject);
	}
}
